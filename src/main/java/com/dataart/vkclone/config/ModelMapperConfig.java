package com.dataart.vkclone.config;

import com.dataart.vkclone.dto.GroupPostDto;
import com.dataart.vkclone.persistence.projection.GroupPostProjection;
import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        modelMapper.addConverter(new AbstractConverter<GroupPostProjection, GroupPostDto>() {
            @Override
            protected GroupPostDto convert(GroupPostProjection postProjection) {
                return GroupPostDto.builder()
                        .id(postProjection.getId())
                        .text(postProjection.getText())
                        .likesCount(postProjection.getLikesCount())
                        .dislikesCount(postProjection.getDislikesCount())
                        .likedByAuthorized(postProjection.getLikedByAuthorized())
                        .userId(postProjection.getUserId())
                        .userLogin(postProjection.getUserLogin())
                        .groupName(postProjection.getGroupName())
                        .groupId(postProjection.getGroupId())
                        .createdAt(postProjection.getCreatedAt().atZone(ZoneId.systemDefault()))
                        .build();
            }
        });
        return modelMapper;
    }

}
