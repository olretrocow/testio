package com.dataart.vkclone.controller;

import com.dataart.vkclone.exception.AlreadyExistsException;
import com.dataart.vkclone.exception.EmptyPostException;
import com.dataart.vkclone.exception.NotFoundException;
import com.dataart.vkclone.exception.PasswordMatchException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ControllerAdvice
@Slf4j
public class ControllerExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public String errorHandler(RuntimeException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.INTERNAL_SERVER_ERROR.value());

        log.error("An unresolved exception occurred.", exception);

        return "error";
    }

    @ExceptionHandler(EmptyPostException.class)
    public String emptyPostExceptionHandler(EmptyPostException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.BAD_REQUEST.value());

        log.error("The post body cannot be empty.", exception);

        return "error";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public String illegalArgumentExceptionHandler(IllegalArgumentException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.BAD_REQUEST.value());

        log.error("Bad request.", exception);

        return "error";
    }

    @ExceptionHandler(BindException.class)
    public String bindExceptionHandler(BindException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.BAD_REQUEST.value());

        log.error("Bind exception.", exception);

        return "error";
    }

    @ExceptionHandler(PasswordMatchException.class)
    public String passwordMatchExceptionHandler(PasswordMatchException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.CONFLICT.value());

        log.error("Password and confirm password does not match.", exception);

        return "error";
    }

    @ExceptionHandler(SecurityException.class)
    public String securityExceptionHandler(SecurityException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.FORBIDDEN.value());

        log.error("Security exception.", exception);

        return "error";
    }

    @ExceptionHandler({NotFoundException.class, UsernameNotFoundException.class})
    public String notFoundExceptionHandler(RuntimeException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.NOT_FOUND.value());

        log.error("Not found.", exception);

        return "error";
    }

    @ExceptionHandler(AlreadyExistsException.class)
    public String alreadyExistsExceptionHandler(AlreadyExistsException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.CONFLICT.value());

        log.error("Already exists.", exception);

        return "error";
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String maxUploadSizeExceededExceptionHandler(MaxUploadSizeExceededException exception, Model model) {
        model.addAttribute("errorStatus", HttpStatus.CONFLICT.value());

        log.error("Max upload size of the file exceeded.", exception);

        return "error";
    }

}
