package com.dataart.vkclone.controller;

import com.dataart.vkclone.dto.*;
import com.dataart.vkclone.service.GroupPostService;
import com.dataart.vkclone.service.GroupService;
import com.dataart.vkclone.service.PostCommentService;
import com.dataart.vkclone.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

@Controller
@RequiredArgsConstructor
public class GroupPostController {

    private final GroupService groupService;
    private final GroupPostService groupPostService;
    private final UserService userService;
    private final PostCommentService postCommentService;

    @Value("${base-url}")
    private String baseUrl;

    @GetMapping("/group/{id}/new")
    public String getNewPost(@PathVariable("id") Long id, Model model) {
        UserDto user = userService.getAuthorisedUser();
        GroupDto group = groupService.getById(id);

        if (!userService.isUserFollowingGroup(group.getId(), user.getId())) {
            throw new SecurityException(String.format(
                    "Only group members can add new posts.\nuserId=%d",
                    user.id));
        }

        model.addAttribute("post", new GroupPostDto());
        model.addAttribute("link", new LinkDto());
        model.addAttribute("group", group);
        model.addAttribute("user", user);
        model.addAttribute("baseUrl", baseUrl);
        return "newPost";
    }

    @PostMapping("/group/{id}/add")
    public String addNewPost(
            @PathVariable Long id,
            @Valid @ModelAttribute("post") GroupPostDto post,
            @RequestParam(name = "imageFiles", required = false) List<MultipartFile> imageFiles) {
        post.setGroup(groupService.getById(id));
        post.setUser(userService.getAuthorisedUser());
        groupPostService.addPost(post, imageFiles);

        return "redirect:/group/{id}";
    }

    @PostMapping("/group/{groupId}/delete/{postId}")
    public String deletePost(@PathVariable Long groupId, @PathVariable Long postId, RedirectAttributes attributes) {
        groupPostService.deletePost(postId);

        attributes.addAttribute("id", groupId);
        return "redirect:/group/{id}";
    }

    @GetMapping("/group/posts/{postId}")
    public String getPost(@PathVariable("postId") Long postId, Model model) {
        UserDto user = userService.getAuthorisedUser();
        GroupPostDto postDto = groupPostService.getById(postId);

        if (!postDto.getUser().id.equals(user.id) && !postDto.getGroup().getOwner().id.equals(user.id)) {
            throw new SecurityException(String.format(
                    "Only the author of a post or the owner of a group can modify posts.\nuserId=%d",
                    user.id));
        }

        model.addAttribute("post", postDto);
        model.addAttribute("group", postDto.getGroup());
        model.addAttribute("user", user);
        return "updatePost";
    }

    @PostMapping("/group/posts")
    public String updatePost(
            @Valid @ModelAttribute("post") GroupPostDto post,
            @RequestParam(name = "imageFiles", required = false) List<MultipartFile> imageFiles,
            RedirectAttributes attributes) {
        GroupPostDto updatedPost = groupPostService.updatePost(post, imageFiles);

        attributes.addAttribute("id", updatedPost.getGroup().getId());
        return "redirect:/group/{id}";
    }

    @PostMapping("/group/posts/react")
    public RedirectView reactOnPost(
            @ModelAttribute("userReaction") UserReactionDto userReaction,
            @RequestHeader(value = "referer", required = false) final String referer) {
        groupPostService.reactOnPost(userReaction);

        return new RedirectView(Objects.requireNonNull(UriComponentsBuilder.fromUriString(referer).build().toUriString()));
    }

    @PostMapping("/group/posts/comment")
    public RedirectView leaveComment(
            @ModelAttribute("comment") PostCommentDto postComment,
            @RequestHeader(value = "referer", required = false) final String referer) {
        postCommentService.leaveComment(postComment);

        return new RedirectView(Objects.requireNonNull(UriComponentsBuilder.fromUriString(referer).build().toUriString()));
    }

    @PostMapping("/group/comments/delete")
    public RedirectView deleteComment(
            @RequestParam(name = "commentId") Long commentId,
            @RequestHeader(value = "referer", required = false) final String referer) {
        postCommentService.deleteComment(commentId);

        return new RedirectView(Objects.requireNonNull(UriComponentsBuilder.fromUriString(referer).build().toUriString()));
    }

}
