package com.dataart.vkclone.controller;

import com.dataart.vkclone.service.GroupPostService;
import com.dataart.vkclone.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class HomeController {

    private final GroupPostService groupPostService;
    private final GroupService groupService;

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("posts", groupPostService.getTop5PostsInThisMonthAndYear());
        model.addAttribute("groups", groupService.getTop4ByMembersCount());
        return "home";
    }

}
