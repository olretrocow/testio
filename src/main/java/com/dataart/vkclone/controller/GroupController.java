package com.dataart.vkclone.controller;

import com.dataart.vkclone.component.DefaultPaginationProperties;
import com.dataart.vkclone.dto.*;
import com.dataart.vkclone.service.GroupPostService;
import com.dataart.vkclone.service.GroupService;
import com.dataart.vkclone.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class GroupController {

    private final GroupService groupService;
    private final GroupPostService groupPostService;
    private final UserService userService;

    private final DefaultPaginationProperties paginationProperties;

    @GetMapping("/groups")
    public String viewAll(
            @RequestParam(value = "groupName", required = false) String groupName,
            @RequestParam(value = "groupsPageNumber", required = false) Integer groupsPageNumber,
            Model model) {
        UserDto user = userService.getAuthorisedUser();

        PageDto groupsPageDto = PageDto.builder()
                .pageNumber(groupsPageNumber != null ? groupsPageNumber : paginationProperties.getPageNumber())
                .pageSize(paginationProperties.getPageSize())
                .sortDirection(paginationProperties.getSortDirection())
                .sortBy("name")
                .build();

        model.addAttribute("user", user);
        model.addAttribute(
                "groups",
                groupName == null || groupName.isBlank() ?
                        groupService.getAll(groupsPageDto) :
                        groupService.searchByName(groupName, groupsPageDto)
        );
        model.addAttribute("groupsPage", groupsPageDto);
        model.addAttribute("searchQueryGroupName", groupName);
        return "groups";
    }

    @GetMapping("/group/{id}")
    public String details(@PathVariable("id") Long id,
                          @RequestParam(value = "postsPageNumber", required = false) Integer postsPageNumber,
                          Model model) {
        UserDto user = userService.getAuthorisedUser();
        GroupDto group = groupService.getById(id);

        PageDto followingsPageDto = PageDto.builder()
                .pageNumber(postsPageNumber != null ? postsPageNumber : paginationProperties.getPageNumber())
                .pageSize(paginationProperties.getPageSize())
                .sortDirection(paginationProperties.getSortDirection())
                .sortBy("created_At")
                .build();

        model.addAttribute("user", user);
        model.addAttribute("isUserFollowing", userService.isUserFollowingGroup(group.getId(), user.getId()));
        model.addAttribute("group", group);
        model.addAttribute("posts", groupPostService.getAllByGroupIdWithAuthorizedUserReactions(id, followingsPageDto));
        model.addAttribute("followingsPage", followingsPageDto);
        model.addAttribute("userReaction", new UserReactionDto());
        model.addAttribute("userComment", new PostCommentDto());
        return "group";
    }

    @PostMapping("/group/join/{id}")
    public String join(@PathVariable("id") Long id, RedirectAttributes attributes) {
        UserDto user = userService.getAuthorisedUser();

        groupService.attachNewMember(user.getId(), id);

        attributes.addAttribute("groupId", id);
        return "redirect:/group/{groupId}";
    }

    @PostMapping("/group/leave/{id}")
    public String leave(@PathVariable("id") Long id, RedirectAttributes attributes) {
        UserDto user = userService.getAuthorisedUser();

        groupService.detachMember(user.getId(), id);

        attributes.addAttribute("groupId", id);
        return "redirect:/group/{groupId}";
    }

    @GetMapping("/groups/new")
    public String getNewGroup(Model model) {
        UserDto user = userService.getAuthorisedUser();

        model.addAttribute("user", user);
        model.addAttribute("group", new GroupDto());
        return "newGroup";
    }

    @PostMapping("/groups/new")
    public String addNewGroup(
            @ModelAttribute GroupDto groupDto,
            RedirectAttributes redirectAttributes) {
        GroupDto addedGroup = groupService.addGroup(groupDto);

        redirectAttributes.addAttribute("groupId", addedGroup.getId());
        return "redirect:/group/{groupId}";
    }

    @PostMapping("/group/delete/{id}")
    public String deleteGroup(@PathVariable Long id) {
        groupService.deleteById(id);

        return "redirect:/feed";
    }

    @GetMapping("/group/edit/{id}")
    public String getGroupForEdit(@PathVariable Long id, Model model) {
        UserDto user = userService.getAuthorisedUser();
        GroupDto group = groupService.getById(id);

        model.addAttribute("user", user);
        model.addAttribute("group", group);
        return "editGroup";
    }

    @PostMapping("/group/edit")
    public String updateGroup(
            @Valid @ModelAttribute("group") GroupDto group,
            @RequestParam(value = "pfp", required = false) MultipartFile groupPicture,
            RedirectAttributes redirectAttributes) {
        groupService.updateGroup(group, groupPicture);

        redirectAttributes.addAttribute("id", group.getId());
        return "redirect:/group/{id}";
    }

    @PostMapping("group/removeGroupPicture/{id}")
    public String removeGroupPicture(@PathVariable Long id, RedirectAttributes redirectAttributes) {
        GroupDto groupDto = groupService.removeGroupPictureById(id);

        redirectAttributes.addAttribute("id", groupDto.getId());
        return "redirect:/group/{id}";
    }

}
