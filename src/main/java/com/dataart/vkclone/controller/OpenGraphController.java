package com.dataart.vkclone.controller;

import com.dataart.vkclone.service.LinkService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OpenGraphController {

    private final LinkService linkService;

    @GetMapping("/share")
    public ResponseEntity<?> getOpenMetaData(@RequestParam(value = "url") String url) {
        return ResponseEntity.ok(linkService.extractLinkDtoFromUrl(url));
    }

}
