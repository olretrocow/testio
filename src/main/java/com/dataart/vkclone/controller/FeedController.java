package com.dataart.vkclone.controller;

import com.dataart.vkclone.component.DefaultPaginationProperties;
import com.dataart.vkclone.dto.PageDto;
import com.dataart.vkclone.dto.PostCommentDto;
import com.dataart.vkclone.dto.UserDto;
import com.dataart.vkclone.dto.UserReactionDto;
import com.dataart.vkclone.service.GroupPostService;
import com.dataart.vkclone.service.GroupService;
import com.dataart.vkclone.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class FeedController {

    @Value("${feed.pageSize}")
    private final int feedPageSize;

    private final GroupService groupService;
    private final UserService userService;
    private final GroupPostService groupPostService;

    private final DefaultPaginationProperties paginationProperties;

    @GetMapping("/feed")
    public String feed(
            @RequestParam(value = "groupName", required = false) String groupName,
            @RequestParam(value = "feedPageNumber", required = false) Integer feedPageNumber,
            @RequestParam(value = "groupPageNumber", required = false) Integer groupPageNumber,
            @RequestParam(value = "searchPageNumber", required = false) Integer searchPageNumber,
            Model model) {
        UserDto user = userService.getAuthorisedUser();

        PageDto.PageDtoBuilder pageDtoBuilder = PageDto.builder();

        PageDto feedPageDto = pageDtoBuilder
                .pageNumber(feedPageNumber != null ? feedPageNumber : paginationProperties.getPageNumber())
                .pageSize(feedPageSize)
                .sortDirection(paginationProperties.getSortDirection())
                .sortBy(paginationProperties.getSortBy())
                .build();

        PageDto groupsPageDto = pageDtoBuilder
                .pageNumber(groupPageNumber != null ? groupPageNumber : paginationProperties.getPageNumber())
                .pageSize(paginationProperties.getPageSize())
                .sortDirection(paginationProperties.getSortDirection())
                .sortBy(paginationProperties.getSortBy())
                .build();

        PageDto searchPageDto = pageDtoBuilder
                .pageNumber(searchPageNumber != null ? searchPageNumber : paginationProperties.getPageNumber())
                .pageSize(paginationProperties.getPageSize())
                .sortDirection(paginationProperties.getSortDirection())
                .sortBy("name")
                .build();

        model.addAttribute("user", user);
        model.addAttribute(
                "search",
                groupName == null || groupName.isBlank() ?
                        null :
                        groupService.searchByName(groupName, searchPageDto)
        );
        model.addAttribute("posts", groupPostService.getAuthorizedUserFeed(feedPageDto));
        model.addAttribute("groups", userService.getAuthorizedUserGroups(groupsPageDto));
        model.addAttribute("feedPage", feedPageDto);
        model.addAttribute("groupsPage", groupsPageDto);
        model.addAttribute("searchPage", searchPageDto);
        model.addAttribute("userReaction", new UserReactionDto());
        model.addAttribute("userComment", new PostCommentDto());
        model.addAttribute("searchQueryGroupName", groupName);
        return "feed";
    }

}
