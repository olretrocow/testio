package com.dataart.vkclone.controller;

import com.dataart.vkclone.component.DefaultPaginationProperties;
import com.dataart.vkclone.dto.ChangePasswordDto;
import com.dataart.vkclone.dto.PageDto;
import com.dataart.vkclone.dto.RegistrationUserDto;
import com.dataart.vkclone.dto.UserDto;
import com.dataart.vkclone.service.GroupPostService;
import com.dataart.vkclone.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final GroupPostService groupPostService;

    private final DefaultPaginationProperties paginationProperties;

    @GetMapping("user/{id}")
    public String userDetails(@PathVariable Long id,
                              @RequestParam(value = "followingsPageNumber", required = false) Integer followingsPageNumber,
                              @RequestParam(value = "userPostsPageNumber", required = false) Integer userPostsPageNumber,
                              Model model) {
        PageDto.PageDtoBuilder pageDtoBuilder = PageDto.builder();

        PageDto followingsPageDto = pageDtoBuilder
                .pageNumber(followingsPageNumber != null ? followingsPageNumber : paginationProperties.getPageNumber())
                .pageSize(paginationProperties.getPageSize())
                .sortDirection(paginationProperties.getSortDirection())
                .sortBy(paginationProperties.getSortBy())
                .build();

        PageDto userPostsPageDto = pageDtoBuilder
                .pageNumber(userPostsPageNumber != null ? userPostsPageNumber : paginationProperties.getPageNumber())
                .pageSize(paginationProperties.getPageSize())
                .sortDirection(paginationProperties.getSortDirection())
                .sortBy(paginationProperties.getSortBy())
                .build();

        model.addAttribute("authorizedUser", userService.getAuthorisedUser());
        model.addAttribute("user", userService.getUserByUserId(id));
        model.addAttribute("userFollowings", userService.getUserGroups(id, followingsPageDto));
        model.addAttribute("userPosts", groupPostService.fetchAllPostsByUserId(id, userPostsPageDto));
        model.addAttribute("followingsPageDto", followingsPageDto);
        model.addAttribute("userPostsPageDto", userPostsPageDto);

        return "user";
    }

    @GetMapping("user")
    public String getUserForEdit(Model model) {
        UserDto user = userService.getAuthorisedUser();

        model.addAttribute("user", user);

        return "userEdit";
    }

    @PostMapping("user")
    public String updateUser(
            @Valid @ModelAttribute("user") UserDto user,
            @RequestParam(value = "pfp", required = false) MultipartFile profilePictureFile,
            RedirectAttributes redirectAttributes) {
        userService.updateUser(user, profilePictureFile);

        redirectAttributes.addAttribute("id", user.id);
        return "redirect:user/{id}";
    }

    @GetMapping("user/changePassword")
    public String getUserForUpdatePassword(Model model) {
        UserDto user = userService.getAuthorisedUser();

        model.addAttribute("changePasswordDto", new ChangePasswordDto());
        model.addAttribute("authorizedUser", user);

        return "changePassword";
    }

    @PostMapping("user/changePassword")
    public String updatePassword(
            @Valid @ModelAttribute("changePasswordDto") ChangePasswordDto changePasswordDto,
            BindingResult errors) {
        if (errors.hasErrors()) {
            throw new IllegalArgumentException(String.format(
                    "The request could not be satisfied.\nuserId=%d",
                    userService.getAuthorisedUser().id));
        }

        userService.updateAuthorizedUserPassword(changePasswordDto);

        return "redirect:/feed";
    }

    @PostMapping("user/removeProfilePicture")
    public String removeProfilePicture(RedirectAttributes redirectAttributes) {
        UserDto userDto = userService.removeAuthorizedUserProfilePicture();

        redirectAttributes.addAttribute("id", userDto.id);
        return "redirect:/user/{id}";
    }

    @GetMapping("/registration")
    public String getNewUser(Model model) {
        model.addAttribute("user", new RegistrationUserDto());

        return "registration";
    }

    @PostMapping("/registration")
    public String addNewUser(
            @Valid @ModelAttribute("user") RegistrationUserDto userDto, Model model) {
        userService.registerNewUser(userDto);

        model.addAttribute("email", userDto.email);
        return "sentEmailConfirmation";
    }

    @GetMapping("/confirm-account")
    public String activateAccount(@RequestParam("token") String confirmationToken) {
        userService.activateAccount(confirmationToken);

        return "accountActivated";
    }

}
