package com.dataart.vkclone.utils;

public class CommonURLUtils {

    public static String addHttpToUrlString(String url) {
        return url.startsWith("https://") ? url : "https://".concat(url);
    }

}
