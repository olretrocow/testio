package com.dataart.vkclone.job;

import com.dataart.vkclone.dto.UserDto;
import com.dataart.vkclone.persistence.projection.GroupProjection;
import com.dataart.vkclone.service.EmailSenderService;
import com.dataart.vkclone.service.GroupService;
import com.dataart.vkclone.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class EmailReminderJob {

    private final GroupService groupService;
    private final UserService userService;
    private final EmailSenderService emailSenderService;

    @Scheduled(cron = "0 59 23 * * *")
    public void remindUsersInactiveWeek() {
        List<GroupProjection> groups = groupService.getBestWeekGroupsInfo();

        if (groups.size() == 0) {
            log.info("No groups found for recommendation.");
            return;
        }
        List<UserDto> users = userService.getAllInactiveForRemind();

        users.forEach(user -> {
            emailSenderService.sendWeeklyBestGroups(user.email, groups);
            userService.setRecommendationEmailDateTime(user.username, LocalDateTime.now());
        });
    }

}
