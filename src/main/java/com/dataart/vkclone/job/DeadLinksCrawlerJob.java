package com.dataart.vkclone.job;

import com.dataart.vkclone.service.LinkService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
@RequiredArgsConstructor
public class DeadLinksCrawlerJob {

    private final LinkService linkService;

    @Scheduled(cron = "0 59 23 * * SUN")
    @Transactional
    public void removeAllUnusedLinks() {
        final long deadLinksCount = linkService.getCount();
        log.info("DeadLinksCrawlerJob has begun. The current number of rows in the table={}", deadLinksCount);
        linkService.removeAllUnusedLinks();
        final long deadLinksCountAfterJob = linkService.getCount();
        log.info("DeadLinksCrawlerJob's complete. Number of rows removed={}", deadLinksCount - deadLinksCountAfterJob);
    }

}
