package com.dataart.vkclone.persistence.repository;

import com.dataart.vkclone.persistence.entity.UserReaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserReactionRepository extends JpaRepository<UserReaction, Long> {

    Optional<UserReaction> getUserReactionByUserIdAndPostId(Long userId, Long postId);

}
