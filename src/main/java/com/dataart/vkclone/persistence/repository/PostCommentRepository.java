package com.dataart.vkclone.persistence.repository;

import com.dataart.vkclone.persistence.entity.PostComment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PostCommentRepository extends JpaRepository<PostComment, Long> {

    List<PostComment> getAllByPostId(Long postId);

}
