package com.dataart.vkclone.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupPostDto {

    private Long id;

    private UserDto user;

    private Long userId;

    private GroupDto group;

    private Long groupId;

    private String userLogin;

    private String groupName;

    @Size(max = 1000)
    private String text;

    private ZonedDateTime createdAt;

    private LinkDto linkAttachment;

    private Boolean likedByAuthorized;

    private List<ImageDto> images;

    private List<PostCommentDto> comments;

    @Min(0)
    private Integer likesCount;

    @Min(0)
    private Integer dislikesCount;

}
