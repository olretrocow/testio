package com.dataart.vkclone.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostCommentDto {

    private Long id;

    private UserDto user;

    private GroupPostDto post;

    private PostCommentDto parent;

    @Size(min = 1, max = 1000)
    private String text;

}
