package com.dataart.vkclone.service.image;

import com.dataart.vkclone.dto.ImageDto;
import com.dataart.vkclone.service.image.util.ImageTransformer;
import lombok.SneakyThrows;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageService {

    @SneakyThrows
    ImageDto add(MultipartFile imageFile, ImageTransformer transformer);

    ImageDto getById(Long id);

    void deleteById(Long id);

    List<ImageDto> getPostImagesByPostId(Long postId);

}
