package com.dataart.vkclone.service;

import com.dataart.vkclone.persistence.entity.User;
import com.dataart.vkclone.persistence.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuditService {

    private final UserRepository userRepository;

    public void checkinUser(String login) {
        User user = userRepository.findByUsername(login)
                .orElseThrow(() -> new UsernameNotFoundException("There is no such user with the received login."));
        user.setLastLogon(LocalDateTime.now());
        userRepository.save(user);
    }

}
