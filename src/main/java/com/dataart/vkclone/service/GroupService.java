package com.dataart.vkclone.service;

import com.dataart.vkclone.dto.GroupDto;
import com.dataart.vkclone.dto.PageDto;
import com.dataart.vkclone.dto.UserDto;
import com.dataart.vkclone.exception.AlreadyExistsException;
import com.dataart.vkclone.exception.NotFoundException;
import com.dataart.vkclone.persistence.entity.Group;
import com.dataart.vkclone.persistence.entity.Image;
import com.dataart.vkclone.persistence.entity.User;
import com.dataart.vkclone.persistence.projection.GroupProjection;
import com.dataart.vkclone.persistence.repository.GroupRepository;
import com.dataart.vkclone.persistence.repository.UserRepository;
import com.dataart.vkclone.service.image.ImageService;
import com.dataart.vkclone.service.image.util.ProfileImageTransformer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class GroupService {

    private final static Pattern FILE_TYPE_IMAGE_CHECK_REGEX = Pattern.compile("([^\\s]+(\\.(?i)(jpe?g|png|gif|bmp))$)");
    private final GroupRepository groupRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final ImageService imageService;
    private final ModelMapper mapper;

    @Value("${search.groups.minLength}")
    private int minLengthSearchQuery;

    public List<GroupDto> getTop4ByMembersCount() {
        return groupRepository.getTop4ByMembersCount().stream()
                .map(group -> mapper.map(group, GroupDto.class))
                .collect(Collectors.toList());
    }

    public Page<GroupDto> searchByName(String name, PageDto pageDto) {
        if (name != null && name.length() < minLengthSearchQuery) {
            throw new IllegalArgumentException("Incorrect search query length.");
        }

        return groupRepository.findByNameContainingIgnoreCase(name, pageDto.getPageable())
                .map(group -> mapper.map(group, GroupDto.class));
    }

    public GroupDto getById(Long id) {
        return mapper.map(
                groupRepository.findById(id).orElseThrow(() ->
                        new NotFoundException("Such a group with the received ID does not exist.")),
                GroupDto.class);
    }

    public void attachNewMember(Long userId, Long groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow(() ->
                new NotFoundException("Such a group with the received ID does not exist."));
        User user = userRepository.findById(userId).orElseThrow(() ->
                new NotFoundException("There is no such user with the received ID."));

        user.getGroups().add(group);

        userRepository.save(user);
    }

    public void detachMember(Long memberId, Long groupId) {
        Group group = groupRepository.findById(groupId).orElseThrow(() ->
                new NotFoundException("Such a group with the received ID does not exist."));
        User user = userRepository.findById(memberId).orElseThrow(() ->
                new NotFoundException("There is no such user with the received ID."));

        user.getGroups().remove(group);

        userRepository.save(user);
    }

    public GroupDto addGroup(GroupDto groupDto) {
        groupRepository.findByName(groupDto.getName())
                .ifPresent(s -> {
                    throw new AlreadyExistsException(String.format(
                            "Group name already exists.\nname=%s",
                            groupDto.getName()));
                });

        UserDto user = userService.getAuthorisedUser();

        if (!user.id.equals(groupDto.getOwnerId())) {
            throw new SecurityException(String.format(
                    "Unexpected behavior.\nuserId=%d",
                    user.id));
        }

        Group group = mapper.map(groupDto, Group.class);
        group.setOwner(mapper.map(user, User.class));
        group.setCreatedAt(ZonedDateTime.now());
        groupRepository.save(group);
        attachNewMember(user.id, group.getId());

        return mapper.map(group, GroupDto.class);
    }

    public void deleteById(Long id) {
        Group group = groupRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Such a group with the received ID does not exist."));
        UserDto user = userService.getAuthorisedUser();

        if (!user.id.equals(group.getOwner().getId())) {
            throw new SecurityException(String.format(
                    "Only the owner of a group can delete it.\nuserId=%d\ngroupId=%d",
                    user.id,
                    group.getId()));
        }

        groupRepository.deleteById(id);
    }

    public List<GroupProjection> getBestWeekGroupsInfo() {
        return groupRepository.getTop5ByMembersThisWeek();
    }

    public Page<GroupDto> getAll(PageDto pageDto) {
        return groupRepository.findAll(pageDto.getPageable())
                .map(group -> mapper.map(group, GroupDto.class));
    }

    public void updateGroup(GroupDto group, MultipartFile groupPicture) {
        Group groupDB = groupRepository.findById(group.getId())
                .orElseThrow(() -> new NotFoundException("There is no such group with the received ID."));

        if (!groupDB.getName().equals(group.getName())) {
            groupRepository.findByName(group.getName())
                    .ifPresent(s -> {
                        throw new AlreadyExistsException(String.format(
                                "Group name already exists.\nname=%s",
                                group.getName()));
                    });
        }

        UserDto user = userService.getAuthorisedUser();

        if (!user.id.equals(group.getOwnerId())) {
            throw new SecurityException(String.format(
                    "Only the owner of a group can edit it.\nuserId=%d\ngroupId=%d",
                    user.id,
                    group.getId()));
        }

        if (groupPicture != null && !groupPicture.isEmpty()) {
            if (!FILE_TYPE_IMAGE_CHECK_REGEX.matcher(Objects.requireNonNull(groupPicture.getOriginalFilename())).matches()) {
                throw new IllegalArgumentException(String.format(
                        "A profile picture can only be an image.\ngroupId=%d",
                        group.getId()));
            }

            if (groupDB.getGroupPicture() != null) {
                imageService.deleteById(groupDB.getGroupPicture().getId());
            }

            groupDB.setGroupPicture(mapper.map(
                    imageService.add(
                            groupPicture,
                            new ProfileImageTransformer()),
                    Image.class));
        }

        groupDB.setName(group.getName());
        groupDB.setStatus(group.getStatus());
        groupDB.setDescription(group.getDescription());

        groupRepository.save(groupDB);
    }

    public GroupDto removeGroupPictureById(Long id) {
        Group group = groupRepository.getById(id);
        UserDto userDto = userService.getAuthorisedUser();

        if (!userDto.id.equals(group.getOwner().getId())) {
            throw new SecurityException(String.format(
                    "Only the owner of a group can remove a group picture.\nuserId=%d\ngroupId=%d",
                    userDto.id,
                    group.getId()));
        }

        if (group.getGroupPicture() != null) {
            imageService.deleteById(group.getGroupPicture().getId());
            group.setGroupPicture(null);
            groupRepository.save(group);
        }

        return mapper.map(group, GroupDto.class);
    }
}
