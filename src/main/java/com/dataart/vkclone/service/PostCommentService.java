package com.dataart.vkclone.service;

import com.dataart.vkclone.dto.GroupPostDto;
import com.dataart.vkclone.dto.PostCommentDto;
import com.dataart.vkclone.dto.UserDto;
import com.dataart.vkclone.exception.EmptyPostCommentException;
import com.dataart.vkclone.exception.NotFoundException;
import com.dataart.vkclone.persistence.entity.PostComment;
import com.dataart.vkclone.persistence.repository.GroupPostRepository;
import com.dataart.vkclone.persistence.repository.PostCommentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PostCommentService {

    private final PostCommentRepository postCommentRepository;

    private final UserService userService;
    private final GroupPostRepository groupPostRepository;

    private final ModelMapper mapper;

    public List<PostCommentDto> getAllByPostId(Long postId) {
        return postCommentRepository.getAllByPostId(postId).stream()
                .map(comment -> mapper.map(comment, PostCommentDto.class))
                .collect(Collectors.toList());
    }

    public void leaveComment(PostCommentDto postCommentDto) {
        if (postCommentDto.getText().isBlank()) {
            throw new EmptyPostCommentException("The post comment body cannot be empty.");
        }

        UserDto user = userService.getAuthorisedUser();
        GroupPostDto post = mapper.map(groupPostRepository.getById(postCommentDto.getPost().getId()), GroupPostDto.class);

        if (!user.id.equals(postCommentDto.getUser().getId())) {
            throw new SecurityException(
                    String.format(
                            "User ID mismatch. The authorized user ID does not match the user ID from the dto.\nauthUserId=%d, userId from dto=%d ",
                            user.id,
                            postCommentDto.getUser().id)
            );
        }

        postCommentDto.setUser(user);
        postCommentDto.setPost(post);
        PostComment comment = mapper.map(postCommentDto, PostComment.class);

        if (postCommentDto.getParent() != null) {
            PostComment parent = postCommentRepository.findById(postCommentDto.getParent().getId())
                    .orElseThrow(() -> new NotFoundException("The comment with the given id was not found."));
            comment.setParent(parent);
        }

        postCommentRepository.save(comment);
    }

    public void deleteComment(Long commentId) {
        UserDto user = userService.getAuthorisedUser();
        PostComment comment = postCommentRepository.getById(commentId);

        if (!comment.getUser().getId().equals(user.id) && !comment.getPost().getGroup().getOwner().getId().equals(user.id)) {
            throw new SecurityException(
                    String.format(
                            "Only the author of a comment or the owner of a group can delete it.\nuserId=%d",
                            user.id)
            );
        }

        postCommentRepository.delete(comment);
    }

}
