package com.dataart.vkclone.service;

import com.dataart.vkclone.dto.*;
import com.dataart.vkclone.exception.AlreadyExistsException;
import com.dataart.vkclone.exception.NotFoundException;
import com.dataart.vkclone.persistence.entity.ConfirmationToken;
import com.dataart.vkclone.persistence.entity.Image;
import com.dataart.vkclone.persistence.entity.User;
import com.dataart.vkclone.persistence.repository.ConfirmationTokenRepository;
import com.dataart.vkclone.persistence.repository.GroupRepository;
import com.dataart.vkclone.persistence.repository.UserRepository;
import com.dataart.vkclone.service.image.ImageService;
import com.dataart.vkclone.service.image.util.ProfileImageTransformer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

    private final static Pattern FILE_TYPE_IMAGE_CHECK_REGEX = Pattern.compile("([^\\s]+(\\.(?i)(jpe?g|png|gif|bmp))$)");

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final ConfirmationTokenRepository confirmationTokenRepository;
    private final ImageService imageService;
    private final EmailSenderService emailSenderService;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper mapper;

    @Value("${scheduling.days-delay}")
    private Integer remindDaysDelay;

    public UserDto getAuthorisedUser() {
        User userDetails = (User) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        return mapper.map(
                userRepository.findByUsername(userDetails.getUsername()).orElseThrow(() ->
                        new UsernameNotFoundException("There is no such user with the received login.")),
                UserDto.class);
    }

    public Boolean isUserFollowingGroup(Long groupId, Long userId) {
        return groupRepository.isUserFollowingGroup(userId, groupId);
    }

    public Page<GroupDto> getUserGroups(Long userId, PageDto pageDto) {
        return groupRepository.getAllUsersGroupsByUserId(userId, pageDto.getPageable())
                .map(group -> mapper.map(group, GroupDto.class));
    }

    public Page<GroupDto> getAuthorizedUserGroups(PageDto pageDto) {
        return getUserGroups(getAuthorisedUser().id, pageDto);
    }

    public String getAuthorizedUserEncryptedPassword() {
        return userRepository.getEncryptedPasswordByUserId(getAuthorisedUser().id);
    }

    public UserDto getUserByUserId(Long userId) {
        return mapper.map(
                userRepository.findById(userId)
                        .orElseThrow(() -> new NotFoundException("There is no such user with the received ID.")),
                UserDto.class);
    }

    public void updateUser(UserDto user, MultipartFile profilePictureFile) {
        User userDB = userRepository.findById(user.getId())
                .orElseThrow(() -> new NotFoundException("There is no such user with the received ID."));

        if (profilePictureFile != null && !profilePictureFile.isEmpty()) {
            if (!FILE_TYPE_IMAGE_CHECK_REGEX.matcher(Objects.requireNonNull(profilePictureFile.getOriginalFilename())).matches()) {
                throw new IllegalArgumentException(String.format(
                        "A profile picture can only be an image\nuserId=%d",
                        user.id));
            }

            if (userDB.getProfilePicture() != null) {
                imageService.deleteById(userDB.getProfilePicture().getId());
            }

            userDB.setProfilePicture(mapper.map(
                    imageService.add(
                            profilePictureFile,
                            new ProfileImageTransformer()),
                    Image.class));
        }

        userDB.setFirstName(user.getFirstName());
        userDB.setMiddleName(user.getMiddleName());
        userDB.setLastName(user.getLastName());
        userDB.setBirthday(user.getBirthday());

        userRepository.save(userDB);
    }

    public UserDto removeAuthorizedUserProfilePicture() {
        User user = userRepository.getById(getAuthorisedUser().getId());

        if (user.getProfilePicture() != null) {
            imageService.deleteById(user.getProfilePicture().getId());
            user.setProfilePicture(null);
            userRepository.save(user);
        }

        return mapper.map(user, UserDto.class);
    }

    public void updateAuthorizedUserPassword(ChangePasswordDto changePasswordDto) {
        UserDto user = getAuthorisedUser();

        if (!changePasswordDto.getUserId().equals(user.id)) {
            throw new SecurityException(String.format(
                    "Only account owner can change the password.\nuserId=%d",
                    user.id));
        }

        if (!passwordEncoder.matches(
                changePasswordDto.getOldPassword(),
                getAuthorizedUserEncryptedPassword())) {
            throw new SecurityException(String.format(
                    "The old password is incorrect.\nuserId=%d",
                    user.id));
        }

        userRepository.updatePasswordByUserId(
                user.id,
                passwordEncoder.encode(changePasswordDto.password));
    }

    public void registerNewUser(RegistrationUserDto userDto) {
        userRepository.findByEmailIgnoreCase(userDto.email)
                .ifPresent(s -> {
                    throw new AlreadyExistsException(String.format(
                            "This email address is already being used.\nuserEmail=%s",
                            userDto.email));
                });
        userRepository.findByUsername(userDto.username)
                .ifPresent(s -> {
                    throw new AlreadyExistsException(String.format(
                            "Username already exists.\nusername=%s",
                            userDto.username));
                });

        User user = mapper.map(userDto, User.class);
        user.setPassword(passwordEncoder.encode(userDto.password));
        user.setCreatedAt(ZonedDateTime.now());
        userRepository.save(user);

        ConfirmationToken confirmationToken = ConfirmationToken.builder()
                .user(user)
                .createdAt(LocalDate.now())
                .build();
        confirmationTokenRepository.save(confirmationToken);

        emailSenderService.sendActivationAccountEmail(
                userDto.getEmail(),
                confirmationToken.getToken().toString()
        );
    }

    public void activateAccount(String confirmationToken) {
        ConfirmationToken token = confirmationTokenRepository.findByToken(UUID.fromString(confirmationToken))
                .orElseThrow(() -> new SecurityException("Token does not match."));

        User user = userRepository.findByUsername(token.getUser().getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("There is no such user with the received login"));

        user.setEnabled(true);
        userRepository.save(user);

        confirmationTokenRepository.delete(token);
    }

    public List<UserDto> getAllInactiveForRemind() {
        return userRepository.getAllInactiveForRemind(remindDaysDelay).stream()
                .map(user -> mapper.map(user, UserDto.class))
                .collect(Collectors.toList());
    }

    public void setRecommendationEmailDateTime(String login, LocalDateTime dateTime) {
        userRepository.setRecommendationEmailDateTime(login, dateTime);
    }

}
