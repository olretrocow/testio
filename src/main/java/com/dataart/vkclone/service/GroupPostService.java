package com.dataart.vkclone.service;

import com.dataart.vkclone.dto.*;
import com.dataart.vkclone.exception.EmptyPostException;
import com.dataart.vkclone.exception.NotFoundException;
import com.dataart.vkclone.persistence.entity.*;
import com.dataart.vkclone.persistence.repository.GroupPostRepository;
import com.dataart.vkclone.persistence.repository.LinkRepository;
import com.dataart.vkclone.persistence.repository.UserReactionRepository;
import com.dataart.vkclone.service.image.ImageService;
import com.dataart.vkclone.service.image.util.PostImageTransformer;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class GroupPostService {

    private final static Pattern FILE_TYPE_IMAGE_CHECK_REGEX = Pattern.compile("([^\\s]+(\\.(?i)(jpe?g|png|gif|bmp))$)");

    private final GroupPostRepository repository;
    private final UserReactionRepository userReactionRepository;
    private final LinkRepository linkRepository;
    private final PostCommentService postCommentService;
    private final UserService userService;
    private final ImageService imageService;
    private final LinkService linkService;
    private final ModelMapper mapper;

    @Value("${post.attachments.maxSize}")
    private final Integer attachmentSize;

    public Page<GroupPostDto> getAllByGroupIdWithAuthorizedUserReactions(Long groupId, PageDto pageDto) {
        UserDto user = userService.getAuthorisedUser();
        return repository.getAllByGroupIdWithAuthorizedUserReactions(groupId, user.id, pageDto.getPageable())
                .map(postProjection -> mapper.map(postProjection, GroupPostDto.class))
                .map(groupPostDto -> {
                    groupPostDto.setImages(imageService.getPostImagesByPostId(groupPostDto.getId()));
                    return groupPostDto;
                })
                .map(groupPostDto -> {
                    groupPostDto.setLinkAttachment(linkService.getRichLinkByPostId(groupPostDto.getId()));
                    return groupPostDto;
                })
                .map(groupPostDto -> {
                    groupPostDto.setComments(postCommentService.getAllByPostId(groupPostDto.getId()));
                    return groupPostDto;
                });
    }

    public Page<GroupPostDto> fetchAllPostsByUserId(Long id, PageDto pageDto) {
        return repository.getAllByUserId(id, pageDto.getPageable())
                .map(groupPost -> mapper.map(groupPost, GroupPostDto.class))
                .map(groupPostDto -> {
                    groupPostDto.setImages(imageService.getPostImagesByPostId(groupPostDto.getId()));
                    return groupPostDto;
                })
                .map(groupPostDto -> {
                    groupPostDto.setLinkAttachment(linkService.getRichLinkByPostId(groupPostDto.getId()));
                    return groupPostDto;
                });
    }

    public List<GroupPostDto> getTop5PostsInThisMonthAndYear() {
        return repository.getTop4WithHighestLikeCountInThisMonth().stream()
                .map(post -> mapper.map(post, GroupPostDto.class))
                .peek(groupPostDto -> groupPostDto.setImages(imageService.getPostImagesByPostId(groupPostDto.getId())))
                .peek(groupPostDto -> groupPostDto.setLinkAttachment(linkService.getRichLinkByPostId(groupPostDto.getId())))
                .collect(Collectors.toList());
    }

    public Page<GroupPostDto> getAuthorizedUserFeed(PageDto pageDto) {
        UserDto user = userService.getAuthorisedUser();
        return repository.getUserFeedByUserId(user.id, pageDto.getPageable())
                .map(postProjection -> mapper.map(postProjection, GroupPostDto.class))
                .map(groupPostDto -> {
                    groupPostDto.setImages(imageService.getPostImagesByPostId(groupPostDto.getId()));
                    return groupPostDto;
                })
                .map(groupPostDto -> {
                    groupPostDto.setLinkAttachment(linkService.getRichLinkByPostId(groupPostDto.getId()));
                    return groupPostDto;
                })
                .map(groupPostDto -> {
                    groupPostDto.setComments(postCommentService.getAllByPostId(groupPostDto.getId()));
                    return groupPostDto;
                });
    }

    public GroupPostDto getById(Long id) {
        return mapper.map(
                repository.findById(id).orElseThrow(() ->
                        new NotFoundException("Such a group post with the received ID does not exist.")),
                GroupPostDto.class);
    }

    public void addPost(GroupPostDto post, List<MultipartFile> images) {
        if (images.size() > attachmentSize) {
            throw new IllegalArgumentException("The number of attachments in the message is exceeded.");
        }

        if (post.getText().isBlank() && images.stream().anyMatch(MultipartFile::isEmpty)) {
            throw new EmptyPostException("The post body cannot be empty.");
        }

        if (!userService.isUserFollowingGroup(post.getGroup().getId(), post.getUser().getId())) {
            throw new SecurityException(String.format(
                    "Only group members can add new posts.\nuserId=%d\ngroupId=%d",
                    post.getUser().id,
                    post.getGroup().getId()));
        }

        List<Image> attachmentImages = savePostImages(post, images).stream()
                .map(imageDto -> mapper.map(imageDto, Image.class))
                .collect(Collectors.toList());

        Link link = null;
        if (post.getLinkAttachment() != null) {
            link = linkRepository
                    .findByUrl(post.getLinkAttachment().getUrl())
                    .orElse(mapper.map(post.getLinkAttachment(), Link.class));

            if (link.getId() == null) {
                linkRepository.save(link);
            }
        }

        GroupPost groupPost = GroupPost.builder()
                .group(mapper.map(post.getGroup(), Group.class))
                .user(mapper.map(post.getUser(), User.class))
                .text(post.getText())
                .images(attachmentImages)
                .linkAttachment(link)
                .createdAt(ZonedDateTime.now())
                .build();

        repository.save(groupPost);
    }

    private List<ImageDto> savePostImages(GroupPostDto post, List<MultipartFile> images) {
        List<ImageDto> attachmentImages = new ArrayList<>();
        if (images.size() > 0 && images.stream().noneMatch(MultipartFile::isEmpty)) {
            images.forEach(image -> {
                if (!FILE_TYPE_IMAGE_CHECK_REGEX.matcher(Objects.requireNonNull(image.getOriginalFilename())).matches()) {
                    throw new IllegalArgumentException(String.format(
                            "An attachment can only be an image.\nuserId=%d",
                            post.getUser().id));
                }
            });

            attachmentImages = images.stream()
                    .filter(Predicate.not(MultipartFile::isEmpty))
                    .map(image -> imageService.add(image, new PostImageTransformer()))
                    .collect(Collectors.toList());
        }
        return attachmentImages;
    }

    public GroupPostDto updatePost(GroupPostDto post, List<MultipartFile> images) {
        if (images.size() > attachmentSize) {
            throw new IllegalArgumentException("The number of attachments in the message is exceeded.");
        }

        GroupPostDto groupPostDB = getById(post.getId());

        if (post.getText().isBlank() && groupPostDB.getImages().size() == 0 && images.stream().anyMatch(MultipartFile::isEmpty)) {
            throw new EmptyPostException("The post body cannot be empty.");
        }

        UserDto user = userService.getAuthorisedUser();

        Link link = null;
        if (post.getLinkAttachment() != null) {
            link = linkRepository
                    .findByUrl(post.getLinkAttachment().getUrl())
                    .orElse(mapper.map(post.getLinkAttachment(), Link.class));

            if (link.getId() == null) {
                linkRepository.save(link);
            }
        }

        if (!groupPostDB.getUser().id.equals(user.id) && !groupPostDB.getGroup().getOwner().id.equals(user.id)) {
            throw new SecurityException(String.format(
                    "Only the author of a post or the owner of a group can modify posts.\nuserId=%d\ngroupId=%d",
                    user.id,
                    groupPostDB.getGroup().getId()));
        }

        if (groupPostDB.getImages().size() > 0 && images.size() > 0 && images.stream().noneMatch(MultipartFile::isEmpty)) {
            groupPostDB.getImages().forEach(image -> imageService.deleteById(image.getId()));
            groupPostDB.setImages(savePostImages(post, images));
        }

        groupPostDB.setText(post.getText());
        groupPostDB.setLinkAttachment(post.getLinkAttachment() == null ? null : mapper.map(link, LinkDto.class));

        return mapper.map(repository.save(mapper.map(groupPostDB, GroupPost.class)), GroupPostDto.class);
    }

    public void deletePost(Long postId) {
        UserDto user = userService.getAuthorisedUser();
        GroupPostDto post = getById(postId);

        if (!post.getUser().id.equals(user.id) && !post.getGroup().getOwner().id.equals(user.id)) {
            throw new SecurityException(String.format(
                    "Only the author of a post or the owner of a group can delete posts.\nuserId=%d",
                    user.id));
        }

        post.getImages().forEach(image -> imageService.deleteById(image.getId()));

        repository.deleteById(postId);
    }

    public void reactOnPost(UserReactionDto userReaction) {
        UserDto user = userService.getAuthorisedUser();
        GroupPostDto post = getById(userReaction.getPostId());

        if (!user.id.equals(userReaction.getUserId())) {
            throw new SecurityException(String.format(
                    "User ID mismatch. The authorized user ID does not match the user ID from the dto.\nauthUserId=%d, userId from dto=%d ",
                    user.id,
                    userReaction.getUserId()));
        }

        Optional<UserReactionDto> reactionDB = userReactionRepository.getUserReactionByUserIdAndPostId(user.getId(), post.getId())
                .map(reaction -> mapper.map(reaction, UserReactionDto.class));

        if (reactionDB.isPresent() && reactionDB.get().isLike() == userReaction.isLike()) {
            removeReactionFromPost(reactionDB.get());
        } else {
            reactionDB.ifPresent(userReactionDto -> userReaction.setId(userReactionDto.getId()));
            userReaction.setUser(user);
            userReaction.setPost(post);
            addReactionOnPost(userReaction);
        }
    }

    private void addReactionOnPost(UserReactionDto userReactionDto) {
        mapper.map(
                userReactionRepository.save(mapper.map(userReactionDto, UserReaction.class)),
                UserReactionDto.class);
    }

    private void removeReactionFromPost(UserReactionDto userReactionDto) {
        userReactionRepository.delete(mapper.map(userReactionDto, UserReaction.class));
    }

}
