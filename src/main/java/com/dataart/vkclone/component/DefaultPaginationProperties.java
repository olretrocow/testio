package com.dataart.vkclone.component;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Data
@Component
public class DefaultPaginationProperties {

    @Value("${pagination.defaultPageNumber}")
    private final int pageNumber;

    @Value("${pagination.defaultPageSize}")
    private final int pageSize;

    @Value("${pagination.defaultSortDirection}")
    private final Sort.Direction sortDirection;

    @Value("${pagination.defaultSortBy}")
    private final String sortBy;

}
