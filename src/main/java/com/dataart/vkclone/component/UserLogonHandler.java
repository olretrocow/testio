package com.dataart.vkclone.component;

import com.dataart.vkclone.service.AuditService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@RequiredArgsConstructor
public class UserLogonHandler implements AuthenticationSuccessHandler {

    private final AuditService auditService;

    @SneakyThrows
    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication) {
        auditService.checkinUser(((UserDetails) authentication.getPrincipal()).getUsername());
        response.sendRedirect("/feed");
    }

}
