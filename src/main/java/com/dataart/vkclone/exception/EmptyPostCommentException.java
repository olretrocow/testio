package com.dataart.vkclone.exception;

public class EmptyPostCommentException extends RuntimeException {

    public EmptyPostCommentException() {
        super();
    }

    public EmptyPostCommentException(String message) {
        super(message);
    }

}
