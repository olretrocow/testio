package com.dataart.vkclone.integration;

import com.dataart.vkclone.component.UserLogonHandler;
import com.dataart.vkclone.config.WebSecurityConfig;
import com.dataart.vkclone.controller.HomeController;
import com.dataart.vkclone.dto.GroupDto;
import com.dataart.vkclone.dto.GroupPostDto;
import com.dataart.vkclone.dto.UserDto;
import com.dataart.vkclone.service.CustomUserDetails;
import com.dataart.vkclone.service.GroupPostService;
import com.dataart.vkclone.service.GroupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@RunWith(SpringRunner.class)
@WebMvcTest(HomeController.class)
@Import(WebSecurityConfig.class)
public class HomeControllerTests {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GroupPostService groupPostService;

    @MockBean
    private GroupService groupService;

    @MockBean
    private UserLogonHandler userLogonHandler;

    @MockBean
    private CustomUserDetails userService;

    @MockBean
    private PasswordEncoder passwordEncoder;


    @Test
    public void index_should_show_5_posts_and_4_groups() throws Exception {

        UserDto userDto = UserDto.builder()
                .id(1L)
                .email("user@mail.com")
                .username("user")
                .build();

        GroupDto groupDto = GroupDto.builder()
                .id(1L)
                .name("group name")
                .owner(userDto)
                .build();

        GroupPostDto postDto = GroupPostDto.builder()
                .groupName("group name")
                .group(groupDto)
                .dislikesCount(0)
                .likesCount(0)
                .text("text")
                .user(userDto)
                .userLogin(userDto.username)
                .build();
        when(groupPostService.getTop5PostsInThisMonthAndYear()).thenReturn(List.of(postDto, postDto, postDto, postDto, postDto));
        when(groupService.getTop4ByMembersCount()).thenReturn(List.of(groupDto, groupDto, groupDto, groupDto));

        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(xpath("//*[@class='post']").nodeCount(5))
                .andExpect(xpath("//*[@class='group']").nodeCount(4));
    }

}
