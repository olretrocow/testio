package com.dataart.vkclone.integration;

import com.dataart.vkclone.controller.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private ControllerExceptionHandler controllerExceptionHandler;

    @Autowired
    private HomeController homeController;

    @Autowired
    private FeedController feedController;

    @Autowired
    private GroupController groupController;

    @Autowired
    private GroupPostController groupPostController;

    @Autowired
    private OpenGraphController openGraphController;

    @Autowired
    private UserController userController;


    @Test
    public void contextLoads() {

        assertThat(controllerExceptionHandler).isNotNull();
        assertThat(homeController).isNotNull();
        assertThat(feedController).isNotNull();
        assertThat(groupController).isNotNull();
        assertThat(openGraphController).isNotNull();
        assertThat(userController).isNotNull();
        assertThat(groupPostController).isNotNull();

    }
}