package com.dataart.vkclone.unit;

import com.dataart.vkclone.service.image.util.ImageTransformer;
import com.dataart.vkclone.service.image.util.ProfileImageTransformer;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class ProfileImageTransformerTests {

    @Test
    public void when_transform_it_should_be_sized() throws IOException {
        // Arrange
        ImageTransformer transformer = new ProfileImageTransformer();
        Resource resource = new ClassPathResource("static/assets/img/robot.png");
        FileInputStream file = new FileInputStream(resource.getFile());
        MultipartFile multipartFile =  new MockMultipartFile("image", "", "image/png" , file.readAllBytes());

        // Act
        BufferedImage image = transformer.transform(multipartFile);

        // Assert
        assertTrue(image.getHeight() == 128 || image.getWidth() == 128);
    }

}
