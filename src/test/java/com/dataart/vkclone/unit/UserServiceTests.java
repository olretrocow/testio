package com.dataart.vkclone.unit;

import com.dataart.vkclone.dto.RegistrationUserDto;
import com.dataart.vkclone.exception.AlreadyExistsException;
import com.dataart.vkclone.persistence.entity.User;
import com.dataart.vkclone.persistence.repository.ConfirmationTokenRepository;
import com.dataart.vkclone.persistence.repository.UserRepository;
import com.dataart.vkclone.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

    @Mock
    private UserRepository userRepository;

    @Mock
    private ConfirmationTokenRepository confirmationTokenRepository;

    @InjectMocks
    private UserService userService;

    @Test(expected = AlreadyExistsException.class)
    public void when_register_user_with_existing_email_it_should_throw_exception() {
        // Assert
        RegistrationUserDto registrationUserDto = RegistrationUserDto.builder()
                .email("test@mail.com")
                .username("test")
                .password("test")
                .confirmPassword("test")
                .build();

        when(userRepository.findByEmailIgnoreCase(any(String.class))).thenReturn(Optional.of(new User()));

        // Act
        userService.registerNewUser(registrationUserDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_activate_account_with_wrong_token_it_should_throw_exception() {
        // Assert
        String wrongToken = "wrong_token";

        when(confirmationTokenRepository.findByToken(UUID.fromString(wrongToken))).thenReturn(Optional.empty());

        // Act
        userService.activateAccount("wrong_token");
    }

    @Test(expected = SecurityException.class)
    public void when_activate_account_with_nonexistent_token_it_should_throw_exception() {
        // Assert
        UUID nonexistentToken = UUID.randomUUID();

        when(confirmationTokenRepository.findByToken(nonexistentToken)).thenReturn(Optional.empty());

        // Act
        userService.activateAccount(nonexistentToken.toString());
    }

}
