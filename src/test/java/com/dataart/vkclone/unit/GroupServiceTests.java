package com.dataart.vkclone.unit;

import com.dataart.vkclone.dto.GroupDto;
import com.dataart.vkclone.dto.UserDto;
import com.dataart.vkclone.persistence.entity.Group;
import com.dataart.vkclone.persistence.entity.Image;
import com.dataart.vkclone.persistence.entity.User;
import com.dataart.vkclone.persistence.repository.GroupRepository;
import com.dataart.vkclone.service.GroupService;
import com.dataart.vkclone.service.UserService;
import com.dataart.vkclone.service.image.ImageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTests {

    @Mock
    private GroupRepository groupRepository;

    @Mock
    private UserService userService;

    @Mock
    private ImageService imageService;

    @Mock
    private ModelMapper mapper;

    @InjectMocks
    private GroupService groupService;

    private UserDto ownerDto;
    private UserDto user;
    private Group group;

    @Before
    public void setUp() {
        this.ownerDto = UserDto.builder()
                .id(1L)
                .email("owner@test.com")
                .username("groupOwner")
                .build();

        this.group = Group.builder()
                .id(1L)
                .name("Test Group")
                .owner(
                        User.builder()
                                .id(1L)
                                .email("owner@test.com")
                                .username("groupOwner")
                                .password("encoded")
                                .build()
                )
                .build();

        this.user = UserDto.builder()
                .id(2L)
                .email("user@test.com")
                .username("user")
                .build();
    }

    @Test(expected = SecurityException.class)
    public void when_remove_group_picture_by_not_owner_it_should_throw_exception() {
        // Arrange
        when(groupRepository.getById(anyLong())).thenReturn(group);
        when(userService.getAuthorisedUser()).thenReturn(user);

        // Act
        groupService.removeGroupPictureById(group.getId());
    }

    @Test
    public void when_remove_group_picture_it_should_be_null() {
        // Arrange
        group.setGroupPicture(
                Image.builder()
                        .id(1L)
                        .width(120)
                        .height(120)
                        .type("image/png")
                        .data(new byte[1])
                        .size(1)
                        .build()
        );
        ModelMapper mapper = new ModelMapper();

        when(groupRepository.getById(anyLong())).thenReturn(group);
        when(userService.getAuthorisedUser()).thenReturn(ownerDto);
        when(this.mapper.map(any(Group.class), eq(GroupDto.class))).thenAnswer(
                (Answer<GroupDto>) invocationOnMock -> {
                    Group group = invocationOnMock.getArgument(0);
                    return mapper.map(group, GroupDto.class);
                });

        // Act
        GroupDto groupDto = groupService.removeGroupPictureById(group.getId());

        // Assert
        assertNull(groupDto.getGroupPicture());
    }

}
